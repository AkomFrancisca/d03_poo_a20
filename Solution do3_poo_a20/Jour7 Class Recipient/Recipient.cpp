#include "Recipient.h"
#include <iostream>
void Recipient::init(double uneCapacite)
{
	Recipient::capacite = 0;
}

Recipient::Recipient()
{
	Recipient::capacite = 0;
	Recipient::volume = 0;
}

Recipient::Recipient(double capacite, double volume)
{
	Recipient::capacite = capacite;
	Recipient::volume = volume;

}

double Recipient::getCapacite()
{
	return Recipient::capacite;
}

double Recipient::getVolume()
{
	return Recipient::volume;
}

void Recipient::vider()
{
	for (int i = capacite; i > 0; i--) {

		Recipient::capacite -= Recipient::volume;
	}
		
}

void Recipient::remplir()
{
	for (int i = 0; i < capacite; i++) {

		Recipient::capacite += Recipient::volume;
	}
}

void Recipient::transvaser(Recipient & dest)
{

}
