#pragma once

class Recipient {

	double capacite;
	double volume;

public:
	
// Les méthodes ou fonctions membres 
	void init(double uneCapacite);
	Recipient();
	Recipient(double capacite, double volume);
	double getCapacite();
	double getVolume();

// Les 3 actions possibles
	void vider(); // vider le récipient
	void remplir(); // Remplir le recipient
	void transvaser(Recipient &dest); // transvaser le recipient dans la destination


};
