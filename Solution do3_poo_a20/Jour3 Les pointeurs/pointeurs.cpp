#include <iostream>
using namespace std;

int main() {

	double x;
	x = 123.456;

	double *ptr_x;
	ptr_x = &x;
	
	cout << " *ptr_x " << *ptr_x << endl;

	*ptr_x = 3.14159;

	cout << " *ptr_x " << *ptr_x << endl;

		system("pause");
		return 0;
}