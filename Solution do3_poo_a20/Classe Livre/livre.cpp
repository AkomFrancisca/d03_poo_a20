#include "livre.h"
#include <iostream>


unsigned short Livre::nbLivres = 0;

Livre::Livre()
{
	std::cout << "Dans le constructeur de Livre" << std::endl;
	Livre::identifiantSysteme = 0;
	Livre::anneeParution = 2000;
	Livre::nbPages = 255;
	Livre::nbAuteurs = 0;
	//strcpy_s(Livre::titre, "titre par defaut");
	//strcpy_s(Livre::ISBN13, "XXX-XXXXXXXXXX");
	Livre::setTitre("titre par defaut");
	Livre::setISBN13("XXX-XXXXXXXXXX");
	Livre::setContributeurs("Nom contributeurs par d�faut");
}

Livre::Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, const char TITRE[], const char ISBN13[)
{
	Livre::identifiantSysteme = identifiantSysteme;
	Livre::anneeParution = anneeParution;
	Livre::nbPages = nbPages;
	Livre::nbAuteurs = nbAuteurs;
	
	Livre::nbLivres = nbLivres;
	Livre::nbLivres++;
	
	//strcpy_s(Livre::titre, TITRE);
	//strcpy_s(Livre::ISBN13, ISBN13);
	Livre::setTitre(TITRE);
	Livre::setISBN13(ISBN13);
}

Livre::Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, unsigned short nbAuteurs, const char TITRE[], const char ISBN13[], const char CONTRIBUTEURS[], unsigned short nbLivres )
{
	Livre::identifiantSysteme = identifiantSysteme;
	Livre::anneeParution = anneeParution;
	Livre::nbPages = nbPages;
	Livre::nbAuteurs = nbAuteurs;
	Livre::nbAuteurs = nbAuteurs;
	Livre::nbLivres = nbLivres;
	Livre::setTitre(TITRE);
	Livre::setISBN13(ISBN13);
	Livre::setContributeurs(CONTRIBUTEURS);
}

unsigned long long Livre::getIdentifiantSysteme() {
	return Livre::identifiantSysteme;
}

short Livre::getAnneeParution() {
	return Livre::anneeParution;
}
unsigned short Livre::getNbPages() {
	return Livre::nbPages;
}

void Livre::setIdentifiantSysteme(unsigned long long identifiantSysteme) {
	Livre::identifiantSysteme = identifiantSysteme;
}
void Livre::setAnneeParution(short anneeParution) {
	Livre::anneeParution = anneeParution;
}
void Livre::setNbPages(unsigned short nbPages) {
	Livre::nbPages = nbPages;
}

unsigned short Livre::getNbLivres()
{
	return Livre::nbLivres;
}

void Livre::setNbLivres(unsigned short nbLivres)
{
	Livre::nbLivres = nbLivres++;
}

const char * Livre::getTitre()
{
	return Livre::titre;
}

void Livre::setTitre(const char TITRE[])
{
	strcpy_s(Livre::titre, TITRE);
}

const char * Livre::getISBN13()
{
	return Livre::ISBN13;
}

void Livre::setISBN13(const char ISBN13[])
{
	strcpy_s(Livre::ISBN13, ISBN13);
}

const char * Livre::getContributeurs()
{
	return Livre::contributeurs;
}

void Livre::setContributeurs(const char CONTRIBUTEURS[])
{
	strcpy_s(Livre::contributeurs, CONTRIBUTEURS);
}

void Livre::lister(const char[])
{
	int i;
	for (i = 0; i < nbContributeurs; i++)
		std::cout << Livre::contributeurs[i] << std::endl;

}

void Livre::afficher()
{
	std::cout << "Identifiant syst�me : " << Livre::getIdentifiantSysteme() << std::endl;
	std::cout << "Titre : " << Livre::getTitre() << std::endl;
	std::cout << "ISBN13 : " << Livre::getISBN13() << std::endl;
	std::cout << "Ann�e de parution : " << Livre::getAnneeParution() << std::endl;
	std::cout << "Nombre de pages : " << Livre::getNbPages() << std::endl;
}

Livre * Livre::creerLivre(Livre livre)
{
	return new Livre;
}




