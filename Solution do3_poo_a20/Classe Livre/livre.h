#pragma once

class Livre {

	unsigned long long identifiantSysteme;
	short anneeParution;
	unsigned short nbPages;
	unsigned short nbAuteurs;
	char titre[201];
	char ISBN13[15];
	unsigned short nbContributeurs;
	char contributeurs[101];
	static unsigned short nbLivres;
	int* livre = new int;


public:

	Livre();
	Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, const char TITRE[], const char ISBN13[]);
	Livre(unsigned long long identifiantSysteme, short anneeParution, unsigned short nbPages, unsigned short nbAuteurs, const char TITRE[], const char ISBN13[], const char contributeurs[]);
	
	static const short tailleTitre;
	static const short tailleISBN13;

	unsigned long long getIdentifiantSysteme();
	void setIdentifiantSysteme(unsigned long long identifiantSysteme);

	short getAnneeParution();
	void setAnneeParution(short anneeParution);

	unsigned short getNbPages();
	void setNbPages(unsigned short nbPages);

	unsigned short getNbLivres();
	void setNbLivres(unsigned short nbLivres);

	const char* getTitre();
	void setTitre(const char TITRE[]);

	const char* getISBN13();
	void setISBN13(const char ISBN13[]);

	const char* getContributeurs();
	void setContributeurs(const char CONTRIBUTEURS[]);

	void lister(const char[]);


	void afficher();

	static int creerLivre();
	

};


