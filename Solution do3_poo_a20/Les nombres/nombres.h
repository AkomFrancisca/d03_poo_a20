#pragma once

class Nombre {
	unsigned int valeur;

public:
	Nombre();
	Nombre(unsigned int valeur);
	unsigned int validerNumerique();
	unsigned int getValeur();
	bool setValeur();

};
